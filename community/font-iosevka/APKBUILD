# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=font-iosevka
pkgver=15.6.3
pkgrel=0
pkgdesc="Versatile typeface for code, from code."
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base
		$pkgname-slab
		$pkgname-curly
		$pkgname-curly-slab
	"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

sha512sums="
915948d683ed03d9240f335bc6782081c68105f2be4e077da6c9980b143319dae7de2e390118be82d299f2419d8f608f963fafc0ef05182a8b983cf021f16a10  super-ttc-iosevka-15.6.3.zip
f8386f2e729e9f62ad2c76e40106b7f5c6c3f0f7ff447d412e1f7ee2cac954ec577852f9d9ff3163b5bc2129560e27ee70604c6dda6b938adebaf28b20ece029  super-ttc-iosevka-slab-15.6.3.zip
84c2456415a9e05d6a7f8f27f5c7dc311e11254582f031b1f33555b5673757782c60198808a3681b50fc671152338e1b1ff15432f7b0c6f681e02e7d293930ae  super-ttc-iosevka-curly-15.6.3.zip
98798fc3a95004d0f7dedf891add3fda6d10654bb8a72a673d46bcfbf68be068d60e3ad635825a02be7acb69abd1df51a3481e523cefad508570a2e88f5723be  super-ttc-iosevka-curly-slab-15.6.3.zip
"
